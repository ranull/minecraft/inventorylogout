package com.rngservers.inventorylogout.events;

import com.rngservers.inventorylogout.inventory.InventoryManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Events implements Listener {
    private InventoryManager inv;

    public Events(InventoryManager inv) {
        this.inv = inv;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        if (player.hasPermission("inventorylogout.inventory")) {
            inv.dropInventory(player);
        }

        if (player.hasPermission("inventorylogout.armor")) {
            inv.dropArmor(player);
        }

        if (player.hasPermission("inventorylogout.exp")) {
            inv.dropEXP(player);
        }
    }
}
