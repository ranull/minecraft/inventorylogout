package com.rngservers.inventorylogout.inventory;

import com.rngservers.inventorylogout.InventoryLogout;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class InventoryManager {
    private InventoryLogout plugin;

    public InventoryManager(InventoryLogout plugin) {
        this.plugin = plugin;
    }

    public void dropInventory(Player player) {
        if (!plugin.getConfig().getBoolean("settings.dropInventory")) {
            return;
        }
        if (!plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
            return;
        }

        int counter = 0;

        for (ItemStack i : player.getInventory().getContents()) {
            if (counter >= 36) {
                return;
            }

            counter++;

            if (i != null) {
                if (plugin.getConfig().getStringList("settings.blacklist").contains(i.getType().toString())) {
                    continue;
                }
                player.getWorld().dropItemNaturally(player.getLocation(), i);
                player.getInventory().remove(i);
            }
        }
    }

    public void dropArmor(Player player) {
        if (!plugin.getConfig().getBoolean("settings.dropArmor")) {
            return;
        }

        if (!plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
            return;
        }

        ItemStack[] armorArray = player.getInventory().getArmorContents();

        for (ItemStack itemStack : armorArray) {
            if (itemStack != null) {
                if (plugin.getConfig().getStringList("settings.blacklist").contains(itemStack.getType().toString())) {
                    continue;
                }

                player.getWorld().dropItemNaturally(player.getLocation(), itemStack);

                itemStack.setAmount(0);
            }
        }
        player.getInventory().setArmorContents(armorArray);
    }

    public void dropEXP(Player player) {
        if (!plugin.getConfig().getBoolean("settings.dropEXP")) {
            return;
        }

        if (!plugin.getConfig().getStringList("settings.gamemode").contains(player.getGameMode().toString())) {
            return;
        }

        player.setTotalExperience(0);
        player.getWorld().spawn(player.getLocation(), ExperienceOrb.class).setExperience(player.getTotalExperience());
    }
}
