package com.rngservers.inventorylogout;

import com.rngservers.inventorylogout.commands.InventoryLogoutCommand;
import com.rngservers.inventorylogout.events.Events;
import com.rngservers.inventorylogout.inventory.InventoryManager;
import org.bukkit.plugin.java.JavaPlugin;

public class InventoryLogout extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();

        InventoryManager inventoryManager = new InventoryManager(this);

        getCommand("inventorylogout").setExecutor(new InventoryLogoutCommand(this));

        getServer().getPluginManager().registerEvents(new Events(inventoryManager), this);
    }
}